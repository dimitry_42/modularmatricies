﻿// LatticeGenerator_Console.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <fstream>
#include <string>
#include <matrix_enumerators.h>
#include <printing.h>

using std::cout;
using std::endl;
using std::cin;

int main()
{
    typedef uint32_t int_t;
    size_t dim = 5;
    int_t det = 10;
    int_t start = 0;
    cout << "dimension determinant start_value : ";
    cin >> dim;
    cin >> det;
    cin >> start;

    std::string out_file_name = "dim" + std::to_string(dim) + "_det" + std::to_string(det)
        + "_st" + std::to_string(start);
    std::ofstream output(out_file_name);
    cout << "count: " <<
        PrintAllLatticies<int_t>(dim, det, output, start) << endl;

    cin.ignore();
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
