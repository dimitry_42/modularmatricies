#pragma once

#include "vector_enumerators.h"

#include <random>

template<typename intT>
class DividersVectorEnumerator :public BasicEnumerationInterface {
public:
    typedef intT integer_type;
    typedef std::vector<integer_type> int_vector_type;

private:
    std::vector<int_vector_type> all_dividers_vectors_;
    integer_type n_;
    typename std::vector<int_vector_type>::const_iterator output_iterator_;

    void RecursiveFill(integer_type part_of_n, int_vector_type* current_dividers_vector) {
        if (part_of_n != 1) {
            for (integer_type i = 2; i <= part_of_n; ++i) {
                integer_type reminder = part_of_n % i;
                integer_type result = static_cast<integer_type>(part_of_n / i);
                if (reminder == 0) {
                    current_dividers_vector->push_back(i);
                    RecursiveFill(result, current_dividers_vector);
                }
            }
        }
        else {
            all_dividers_vectors_.push_back(*current_dividers_vector);
        }
        if (!current_dividers_vector->empty())
            current_dividers_vector->pop_back();
    }

public:
    explicit DividersVectorEnumerator(integer_type n)
        :n_(n) {}

    void Init() {
        int_vector_type current_dividers_vector;
        RecursiveFill(n_, &current_dividers_vector);
        output_iterator_ = all_dividers_vectors_.cbegin();
    }

    const int_vector_type& GetCurrentDividersVector() const {
        return *output_iterator_;
    }

    bool is_max() const {
        return output_iterator_ + 1 == all_dividers_vectors_.cend();
    }

    bool next() override {
        ++output_iterator_;
        if (output_iterator_ == all_dividers_vectors_.cend()) {
            output_iterator_ = all_dividers_vectors_.cbegin();
            return false;
        }
        return true;
    }

    template<typename random_engineT>
    const int_vector_type& GenerateRandomDividersVector(random_engineT & random_engine) const {

        std::uniform_int_distribution<size_t> dist(0, all_dividers_vectors_.size() );
        return all_dividers_vectors_[dist(random_engine)];
    }
};