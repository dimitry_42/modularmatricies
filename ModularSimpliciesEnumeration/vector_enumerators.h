#pragma once

#include <vector>
#include <iterator>
#include <numeric>

template<typename valueT>
class StandardCheker {
public:
    typedef valueT value_type;
    typedef std::vector<value_type> vector_type;

private:
    vector_type min_values_, max_values_;

public:
    StandardCheker() {}
    
    StandardCheker(vector_type min_values, vector_type max_values)
        :min_values_(std::move(min_values)),
        max_values_(std::move(max_values))
    {}

    //StandardCheker(const StandardCheker& other)
    //    :min_values_(other.min_values_),
    //    max_values_(other.max_values_)
    //{}

    bool is_max(size_t i, const value_type& val) const { return max_values_[i] == val; }
    bool is_min(size_t i, const value_type& val) const { return min_values_[i] == val; }
    void set_min(size_t i, value_type* val) const { *val = min_values_[i]; }
    void set_max(size_t i, value_type* val) const { *val = max_values_[i]; }
};

class CycleChecker {
public:

    template<typename valueT>
    bool is_max(size_t i, const valueT& val) const { return val.is_max(); }

    template<typename valueT>
    bool is_min(size_t i, const valueT& val) const { return val.is_min(); }

    template<typename valueT>
    void set_min(size_t i, valueT* val) const { ++(*val); }

    template<typename valueT>
    void set_max(size_t i, valueT* val) const { --(*val); }
};

class BasicEnumerationInterface {
public:
    bool is_max()const;
    bool is_min()const;
    virtual bool next() = 0;

    BasicEnumerationInterface& operator++() {
        next();
        return *this;
    }

    virtual void Init() {}

    /*BaseEnumVector operator++(int) const {
        LexVector copy;
        ++(*this);
        return std::move(copy);
    }*/
};

template<typename valueT, typename chekerT >
class BaseEnumVector :public std::vector<valueT>, public BasicEnumerationInterface {
public:
    typedef std::vector<valueT> vector_type;
    typedef chekerT cheker_type;

private:
    typedef vector_type Base;
    bool is_max_, is_min_;

protected:
    cheker_type cheker_;

public:

    explicit BaseEnumVector(cheker_type cheker = chekerT())
        :Base(), cheker_(cheker), is_max_(false), is_min_(false) {}

    BaseEnumVector(vector_type starting_values, cheker_type cheker = chekerT())
        :Base(std::move(starting_values)),
        cheker_(cheker), is_max_(false), is_min_(false)
    {}

    bool is_max()const {
        for (auto i = Base::rbegin(); i != Base::rend(); ++i)
            if (!cheker_.is_max(Base::rend() - i - 1, *i))
                return false;
        return true;
    }
    bool is_min()const {
        return is_min_;
        for (auto i = Base::rbegin(); i != Base::rend(); ++i)
            if (!cheker_.is_min(Base::rend() - i - 1, *i))
                return false;
        return true;
    }
};

template<typename valueT, typename chekerT >
class LexVector :public BaseEnumVector<valueT, chekerT> {
private:
    typedef BaseEnumVector<valueT, chekerT> Base;

public:
    using typename Base::cheker_type;
    using typename Base::vector_type;

    explicit LexVector(cheker_type cheker = chekerT())
        :Base(cheker) {}

    LexVector(vector_type starting_values, cheker_type cheker = chekerT())
        :Base(std::move(starting_values), cheker)
    {}

    bool next() override {
        auto i = Base::rbegin();
        for (; i != Base::rend(); ++i) {
            size_t pos = Base::rend() - i - 1;
            if (Base::cheker_.is_max(pos, *i)) {
                Base::cheker_.set_min(pos, &(*i));
            }
            else break;
        }

        if (i == Base::rend())
            return false;

        ++(*i);

        return true;
    }
};

template<typename valueT, typename chekerT >
class SortVector :public BaseEnumVector<valueT, chekerT> {
private:
    typedef BaseEnumVector<valueT, chekerT> Base;

public:
    using typename Base::cheker_type;
    using typename Base::vector_type;

    explicit SortVector(cheker_type cheker = chekerT())
        :Base(cheker) {}

    SortVector(vector_type starting_values, cheker_type cheker = chekerT())
        :Base(std::move(starting_values), cheker)
    {}

    bool next() override {
        auto i = Base::rbegin();
        for (; i != Base::rend(); ++i)
            if (!Base::cheker_.is_max(Base::rend() - i - 1, *i))
                break;

        if (i == Base::rend())
        {
            for (auto j = Base::begin(); j != Base::end(); ++j)
                Base::cheker_.set_min(j - Base::begin(), &(*j));
            return false;
        }

        ++(*i);

        if (i != Base::rbegin())
            for (auto j = i.base(); j != Base::end(); ++j)
                *j = *i;

        return true;
    }
};