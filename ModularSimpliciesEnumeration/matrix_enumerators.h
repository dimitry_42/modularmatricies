#pragma once

#include "vector_enumerators.h"
#include "dividers_vectors_enumerator.h"

#include <cmath>
#include <random>

template<typename intT>
class SquareHermiteMatrixRandomGenerator_PrescribedDiagonal {
public:
	typedef intT integer_type;
	typedef std::vector<integer_type> int_vector_type;

private:
	size_t n_;
	int_vector_type non_unit_diag_, non_unit_diag_decreased_;
	integer_type determinant_;
	integer_type starting_value_;

	void ComputeDet() { determinant_ = std::accumulate(non_unit_diag_.begin(), non_unit_diag_.end(), 1, std::multiplies<integer_type>()); }
	void BuildRedusedDiagonal() {
		non_unit_diag_decreased_ = non_unit_diag_;
		for (auto& val : non_unit_diag_decreased_)
			--val;
	}

public:
	size_t get_diag_units_num() const { return n_ - non_unit_diag_.size(); }
	size_t get_size() const { return n_; }
	size_t get_diag_nonunits_num() const { return non_unit_diag_.size(); }
	integer_type get_determinant() const { return determinant_; }

	SquareHermiteMatrixRandomGenerator_PrescribedDiagonal(size_t n, int_vector_type non_unit_diag, integer_type starting_value = 0)
		:n_(n), determinant_(0), non_unit_diag_(std::move(non_unit_diag)), starting_value_(starting_value)
	{}

	void Init() {
		ComputeDet();
		BuildRedusedDiagonal();
	}

	template<typename random_generatorT>
	std::vector<int_vector_type> GenerateRandomRowsMatrix(random_generatorT& random_generator) const {
		std::vector<int_vector_type> result(n_, int_vector_type(n_, 0));

		for (size_t i = 0; i < get_diag_units_num(); ++i)
			result[i][i] = 1;

		for (size_t i = get_diag_units_num(); i < n_; ++i)
			result[i][i] = non_unit_diag_[i - get_diag_units_num()];

		for (size_t row_num = get_diag_units_num(); row_num < n_; ++row_num) {
			std::uniform_int_distribution<integer_type> dist(starting_value_,
				non_unit_diag_decreased_[row_num - get_diag_units_num()]);

			for (size_t column_num = 0; column_num < row_num; ++column_num)
				result[row_num][column_num] = dist(random_generator);
		}

		return std::move(result);
	}

	template<typename random_generatorT>
	std::vector<int_vector_type> GenerateRandomColumnsMatrix(random_generatorT& random_generator) const {
		std::vector<int_vector_type> result(n_, int_vector_type(n_, 0));

		for (size_t i = 0; i < get_diag_units_num(); ++i)
			result[i][i] = 1;

		for (size_t i = get_diag_units_num(); i < n_; ++i)
			result[i][i] = non_unit_diag_[i - get_diag_units_num()];

		for (size_t row_num = get_diag_units_num(); row_num < n_; ++row_num) {
			std::uniform_int_distribution<integer_type> dist(starting_value_,
				non_unit_diag_decreased_[row_num - get_diag_units_num()]);

			for (size_t column_num = 0; column_num < row_num; ++column_num)
				result[column_num][row_num] = dist(random_generator);
		}

		return std::move(result);
	}
};

template<typename intT>
class SquareHermiteMatrixRandomGenerator {
public:
	typedef intT integer_type;
	typedef std::vector<integer_type> int_vector_type;

private:
	size_t n_;
	integer_type determinant_;
	integer_type starting_value_;
	DividersVectorEnumerator<integer_type> dividers_generator_;

public:
	size_t get_size() const { return n_; }
	integer_type get_determinant() const { return determinant_; }

	SquareHermiteMatrixRandomGenerator(size_t n, integer_type determinant, integer_type starting_value = 0)
		:n_(n), determinant_(determinant), dividers_generator_(determinant), starting_value_(starting_value)
	{}

	void Init() {
		dividers_generator_.Init();
	}

	template<typename random_generatorT>
	SquareHermiteMatrixRandomGenerator_PrescribedDiagonal<integer_type>
		GetRandomGeneratorWithPrescribedDiagonal(random_generatorT& random_generator) const {
		SquareHermiteMatrixRandomGenerator_PrescribedDiagonal<integer_type> result(n_, 
			dividers_generator_.GenerateRandomDividersVector(random_generator), starting_value_);
		
		result.Init();

		return std::move(result);
	}

	template<typename random_generatorT>
	std::vector<int_vector_type> GenerateRowsMatrix(random_generatorT& random_generator) const {
		SquareHermiteMatrixRandomGenerator_PrescribedDiagonal<integer_type> gen = std::move(GetRandomGeneratorWithPrescribedDiagonal(random_generator));
		return std::move(gen.GenerateRandomRowsMatrix(random_generator));
	}

	template<typename random_generatorT>
	std::vector<int_vector_type> GenerateColumnsMatrix(random_generatorT& random_generator) const {
		SquareHermiteMatrixRandomGenerator_PrescribedDiagonal<integer_type> gen = std::move(GetRandomGeneratorWithPrescribedDiagonal(random_generator));
		return std::move(gen.GenerateRandomColumnsMatrix(random_generator));
	}
};

template<typename intT>
class SquareHermiteMatrixEnumerator_PrescribedDiagonal
	:public BasicEnumerationInterface {
public:
	typedef intT integer_type;
	typedef std::vector<integer_type> int_vector_type;

private:
	typedef LexVector<integer_type, StandardCheker<integer_type>> Column;
	typedef SortVector<Column, CycleChecker> BMatrix;
	typedef LexVector<Column, CycleChecker> RMatrix;

	BMatrix B_;
	RMatrix R_;
	size_t n_;
	int_vector_type non_unit_diag_, non_unit_diag_decreased_;
	integer_type determinant_;
	integer_type starting_value_;

	void ComputeDet() { determinant_ = std::accumulate(non_unit_diag_.begin(), non_unit_diag_.end(), 1, std::multiplies<integer_type>()); }
	void BuildRedusedDiagonal() {
		non_unit_diag_decreased_ = non_unit_diag_;
		for (auto& val : non_unit_diag_decreased_)
			--val;
	}
	void InitB() {
		int_vector_type starting_int_vector = int_vector_type(get_diag_nonunits_num(), starting_value_);
		StandardCheker<integer_type> columns_checker = StandardCheker<integer_type>(starting_int_vector, non_unit_diag_decreased_);
		Column init_column = Column(starting_int_vector, columns_checker);
		B_.assign(get_diag_units_num(), init_column);
	}
	void InitR() {
		for (size_t diag_index = 0; diag_index < get_diag_nonunits_num(); ++diag_index) {
			size_t column_size = get_diag_nonunits_num() - diag_index - 1;
			int_vector_type starting_int_vector = int_vector_type(column_size, starting_value_);
			int_vector_type end_int_vector = int_vector_type(non_unit_diag_decreased_.begin() + diag_index + 1, non_unit_diag_decreased_.end());
			StandardCheker<integer_type> checker = StandardCheker<integer_type>(starting_int_vector, end_int_vector);
			R_.push_back(Column(starting_int_vector, checker));
		}
	}

public:

	size_t get_diag_units_num() const { return n_ - non_unit_diag_.size(); }
	size_t get_size() const { return n_; }
	size_t get_diag_nonunits_num() const { return non_unit_diag_.size(); }
	integer_type get_determinant() const { return determinant_; }

	SquareHermiteMatrixEnumerator_PrescribedDiagonal(size_t n, int_vector_type non_unit_diag, integer_type starting_value = 0)
		:n_(n), determinant_(0), non_unit_diag_(std::move(non_unit_diag)), starting_value_(starting_value)
	{}

	void Init()
	{
		ComputeDet();
		BuildRedusedDiagonal();

		InitB();
		InitR();
	}

	bool next() override {
		if (!R_.next()) {
			return B_.next();
		}
		return true;
	}

	bool is_max() const {
		return R_.is_max() && B_.is_max();
	}

	std::vector<int_vector_type> GenerateRowsMatrix() const {
		std::vector<int_vector_type> result(n_, int_vector_type(n_, 0));

		for (size_t i = 0; i < get_diag_units_num(); ++i)
			result[i][i] = 1;

		for (size_t i = get_diag_units_num(); i < n_; ++i)
			result[i][i] = non_unit_diag_[i - get_diag_units_num()];

		for (size_t column_num = 0; column_num < get_diag_units_num(); ++column_num)
			for (size_t row_num = get_diag_units_num(); row_num < n_; ++row_num)
				result[row_num][column_num] = B_[column_num][row_num - get_diag_units_num()];

		for (size_t column_num = get_diag_units_num(); column_num < n_; ++column_num)
			for (size_t row_num = column_num + 1; row_num < n_; ++row_num)
				result[row_num][column_num] = R_[column_num - get_diag_units_num()][row_num - column_num - 1];

		return std::move(result);
	}

	std::vector<int_vector_type> GenerateColumnsMatrix() const {
		std::vector<int_vector_type> result(n_, int_vector_type(n_, 0));

		for (size_t i = 0; i < get_diag_units_num(); ++i)
			result[i][i] = 1;

		for (size_t i = get_diag_units_num(); i < n_; ++i)
			result[i][i] = non_unit_diag_[i - get_diag_units_num()];

		for (size_t column_num = 0; column_num < get_diag_units_num(); ++column_num)
			for (size_t row_num = get_diag_units_num(); row_num < n_; ++row_num)
				result[column_num][row_num] = B_[column_num][row_num - get_diag_units_num()];

		for (size_t column_num = get_diag_units_num(); column_num < n_; ++column_num)
			for (size_t row_num = column_num + 1; row_num < n_; ++row_num)
				result[column_num][row_num] = R_[column_num - get_diag_units_num()][row_num - column_num - 1];

		return std::move(result);
	}
};

template<typename intT>
class SquareHermiteMatrixEnumerator :public BasicEnumerationInterface {
public:
	typedef intT integer_type;
	typedef std::vector<integer_type> int_vector_type;

private:
	typedef SquareHermiteMatrixEnumerator_PrescribedDiagonal<integer_type> MtEnum_PrescribedDiagonal;
	std::unique_ptr<MtEnum_PrescribedDiagonal> mt_enum_prescribed_diagonal_;
	DividersVectorEnumerator<integer_type> diagonal_enumerator_;
	integer_type n_;
	integer_type starting_value_;

	bool GenerateNextDiagonal() {
		bool has_next_diagonal = diagonal_enumerator_.next();
		mt_enum_prescribed_diagonal_.reset(new MtEnum_PrescribedDiagonal(
			get_size(), diagonal_enumerator_.GetCurrentDividersVector(), starting_value_
		));
		mt_enum_prescribed_diagonal_->Init();
		return has_next_diagonal;
	}

public:
	size_t get_diag_units_num() const { mt_enum_prescribed_diagonal_->get_diag_units_num(); }
	size_t get_size() const { return n_; }
	size_t get_diag_nonunits_num() const { return mt_enum_prescribed_diagonal_->get_diag_nonunits_num(); }
	integer_type get_determinant() const { return mt_enum_prescribed_diagonal_->get_determinant(); }

	int_vector_type get_nonunits_diag()const { return std::move(diagonal_enumerator_.GetCurrentDividersVector()); }
	int_vector_type get_full_diag()const {
		int_vector_type result(1, get_diag_units_num());
		int_vector_type nonunits_part = std::move(get_nonunits_diag());
		result.insert(result.end(), nonunits_part.begin(), nonunits_part.end());
		return std::move(result);
	}

	SquareHermiteMatrixEnumerator(integer_type n, integer_type det, integer_type starting_value = 0)
		:diagonal_enumerator_(det), n_(n), starting_value_(starting_value) {}

	void Init() {
		diagonal_enumerator_.Init();
		mt_enum_prescribed_diagonal_ = std::make_unique<MtEnum_PrescribedDiagonal>(MtEnum_PrescribedDiagonal(
			get_size(), diagonal_enumerator_.GetCurrentDividersVector(), starting_value_
		));
		mt_enum_prescribed_diagonal_->Init();
	}

	bool next() override {
		if (!mt_enum_prescribed_diagonal_->next()) {
			return GenerateNextDiagonal();
		}
		return true;
	}

	bool is_max() const {
		return mt_enum_prescribed_diagonal_->is_max() && diagonal_enumerator_.is_max();
	}

	std::vector<int_vector_type> GenerateRowsMatrix() const {
		return std::move(mt_enum_prescribed_diagonal_->GenerateRowsMatrix());
	}
};

enum class EnumerationType { from_determinant, prescribed_diagonal };

//template<typename intT, typename ratT = long double>
//class SimplexMatrixEnumerator :public BasicEnumerationInterface {
//public:
//    typedef intT integer_type;
//    typedef std::vector<integer_type> int_vector_type;
//
//private:
//    typedef ratT rational_type;
//    typedef std::vector<rational_type> rational_vector_type;
//
//    std::unique_ptr<BasicEnumerationInterface> square_matrix_enum_;
//    std::vector<int_vector_type> additional_row_candidates_;
//    typename std::vector<int_vector_type>::const_iterator add_row_candidates_iterator_;
//    std::vector<int_vector_type> current_square_matrix_;
//
//    void FillAddRowCandidates(size_t i, rational_vector_type* x, int_vector_type * y) {
//        size_t n = get_columns_num();
//        rational_type r_i = 0;
//        for (size_t k = i + 1; i < n; ++k)
//            r_i += (*x)[k] * current_square_matrix_[k][i];
//        integer_type ceil_r_i = static_cast<integer_type>( ceil(r_i) );
//        integer_type initial_y = ceil_r_i;
//        if (ceil_r_i == r_i) ++initial_y;
//        integer_type H_ii = current_square_matrix_[i][i];
//        for (integer_type y_i = initial_y; i < initial_y + H_ii; ++y_i) {
//            x_i = (y_i - r_i) / H_ii;
//            (*x)[i] = x_i;
//            (*y)[i] = y_i;
//            if (i != 0)
//                FillAddRowCandidates(i - 1, x, y);
//            else
//                additional_row_candidates_.push_back(*y);
//        }
//    }
//
//    void FillAddRowCandidates() {
//        size_t n = get_columns_num();
//        rational_vector_type x(0.0,n);
//        int_vector_type y(0, n);
//        FillAddRowCandidates(n-1, &x, &y);
//
//        //negation of columns of additional_row_candidates_
//        for (auto& row : additional_row_candidates_)
//            for (auto& x : row)
//                x = -x;
//    }
//
//public:
//    size_t get_columns_num() const { return square_matrix_enum_->get_size(); }
//    size_t get_rows_num() const { return get_columns_num() + 1; }
//    integer_type get_max_minor() const { return get_determinant(); }
//
//    size_t get_diag_units_num() const { square_matrix_enum_->get_diag_units_num(); }
//    size_t get_diag_nonunits_num() const { return square_matrix_enum_->get_diag_nonunits_num(); }
//    integer_type get_determinant() const { return mt_enum_prescribed_diagonal_->get_determinant(); }
//
//    int_vector_type get_nonunits_diag()const { return std::move(square_matrix_enum_->get_nonunits_diag());  }
//    int_vector_type get_full_diag()const { return std::move(square_matrix_enum_->get_full_diag()); }
//
//    SimplexMatrixEnumerator(size_t n, integer_type max_minor)
//        :square_matrix_enum_(new SquareHermiteMatrixEnumerator(n, max_minor))
//    {}
//
//    SimplexMatrixEnumerator(size_t n, int_vector_type non_unit_diagonal)
//        :square_matrix_enum_(new SquareHermiteMatrixEnumerator_PrescribedDiagonal(n, std::move(non_unit_diagonal)))
//    {}
//
//    void Init() {
//        square_matrix_enum_->Init();
//        current_square_matrix_ = std::move(square_matrix_enum_->GenerateRowsMatrix());
//        void FillAddRowCandidates();
//        add_row_candidates_iterator_ = additional_row_candidates_.cbegin()
//    }
//
//    std::vector<int_vector_type> GenerateRowsMatrix() const {
//        std::vector<int_vector_type> result = current_square_matrix_;
//        result.push_back(*add_row_candidates_iterator_);
//        return std::move(result);
//    }
//
//    bool next() {
//        ++add_row_candidates_iterator_;
//        if (add_row_candidates_iterator_ == additional_row_candidates_.cend()) {
//            bool result = square_matrix_enum_->next();
//            current_square_matrix_ = std::move(square_matrix_enum_->GenerateRowsMatrix());;
//            FillAddRowCandidates();
//            add_row_candidates_iterator_ = additional_row_candidates_.cbegin();
//            return result;
//        }
//        return true;
//    }
//
//    bool is_max() const {
//        return square_matrix_enum_->is_max() && (add_row_candidates_iterator_+1 == additional_row_candidates_.end() );
//    }
//};
//
//template<typename intT, typename ratT = long double>
//class SimplexExtendentMatrixEnumerator :public BasicEnumerationInterface {
//public:
//    typedef intT integer_type;
//    typedef std::vector<integer_type> int_vector_type;
//
//private:
//    typedef SimplexMatrixEnumerator<intT, ratT> MatrixEnumerator;
//    typedef StandardCheker<intT> RHS_Cheker;
//    typedef LexVector<intT, RHS_Cheker> RHS_Enumerator;
//
//    MatrixEnumerator matrix_enumerator_;
//    RHS_Enumerator rhs_enumerator_;
//    integer_type width_param_;
//
//    void Init_RhsEnumerator() {
//        size_t n = get_dimension();
//        int_vector_type rhs_bound_vector_ = std::move(get_full_diag());
//        rhs_bound_vector_.push_back(width_param_);
//        RHS_Cheker rhs_cheker(int_vector_type(n + 1, 0), rhs_bound_vector_);
//        rhs_enumerator_ = RHS_Enumerator(n + 1, rhs_cheker);
//    }
//
//public:
//    size_t get_dimension() const { return matrix_enumerator_.get_columns_num(); }
//    size_t get_inequalities_num() const { return matrix_enumerator_.get_rows_num(); }
//    integer_type get_max_minor() const { return get_determinant(); }
//
//    size_t get_diag_units_num() const { matrix_enumerator_.get_diag_units_num(); }
//    size_t get_diag_nonunits_num() const { return matrix_enumerator_.get_diag_nonunits_num(); }
//    integer_type get_determinant() const { return matrix_enumerator_.get_determinant(); }
//
//    int_vector_type get_nonunits_diag()const { return std::move(matrix_enumerator_.get_nonunits_diag()); }
//    int_vector_type get_full_diag()const { return std::move(matrix_enumerator_.get_full_diag());  }
//
//    SimplexExtendentMatrixEnumerator(size_t n, integer_type max_minor, integer_type width_param = max_minor)
//        :MatrixEnumerator(n, max_minor), width_param_(width_param) {}
//
//    SimplexExtendentMatrixEnumerator(size_t n, int_vector_type non_unit_diagonal, integer_type width_param)
//        :MatrixEnumerator(n, std::move(non_unit_diagonal)), width_param_(width_param) {}
//
//    void Init() {
//        matrix_enumerator_.Init();
//        Init_RhsEnumerator();
//    }
//
//    std::vector<int_vector_type> GenerateRowsMatrix() const {
//        std::move(matrix_enumerator_.GenerateRowsMatrix());
//    }
//
//    const int_vector_type& get_rhs() const { return rhs_enumerator_; }
//
//    std::vector<int_vector_type> GenerateRowsExtendentMatrix() const {
//        auto result = GenerateRowsMatrix();
//        const int_vector_type & rhs = get_rhs();
//        size_t m = get_inequalities_num();
//        for (size_t i = 0; i < m; ++i)
//            result.push_back(rhs[i]);
//        return std::move(result);
//    }
//
//    std::vector<int_vector_type> GenerateDDMExtendentMatrix() const {
//        auto result = GenerateRowsMatrix();
//        const int_vector_type& rhs = get_rhs();
//        size_t m = get_inequalities_num();
//        for (size_t i = 0; i < m; ++i)
//            result.push_back(-rhs[i]);
//        return std::move(result);
//    }
//
//    bool next() {
//        if (!rhs_enumerator_.next()) {
//            bool result matrix_enumerator_.next();
//            Init_RhsEnumerator();
//            return result;
//        }
//        return true;
//    }
//
//    bool is_max() const {
//        return matrix_enumerator_.is_max() && rhs_enumerator_.is_max();
//    }
//};