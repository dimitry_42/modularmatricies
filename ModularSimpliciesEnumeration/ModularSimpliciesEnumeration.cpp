﻿// ModularSimpliciesEnumeration.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <fstream>
#include <string>
#include "matrix_enumerators.h"
#include "printing.h"

using std::cout;
using std::endl;
using std::cin;

void square_lattice_random_generator_test() {
    typedef size_t int_type;
    std::mt19937 rg;
    SquareHermiteMatrixRandomGenerator<int_type> gen_from_det(10, 100);
    gen_from_det.Init();
    SquareHermiteMatrixRandomGenerator_PrescribedDiagonal<int_type> gen_from_diag = 
        std::move(gen_from_det.GetRandomGeneratorWithPrescribedDiagonal(rg));
    cout << gen_from_diag.get_determinant() << ' ' << gen_from_diag.get_size() << endl;
    PrintRowMatrix(gen_from_diag.GenerateRandomRowsMatrix(rg));
    cout << endl << endl;
    PrintRowMatrix(gen_from_det.GenerateRowsMatrix(rg));
    cout << endl << endl;
    PrintRowMatrix(gen_from_det.GenerateColumnsMatrix(rg));
}

int main()
{
    square_lattice_random_generator_test();
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
