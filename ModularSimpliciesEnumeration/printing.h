#pragma once

#include "vector_enumerators.h"
#include "matrix_enumerators.h"

template<typename valueT, typename chekerT>
std::ostream& operator<< (std::ostream& out, const BaseEnumVector<valueT, chekerT>& v) {
    if (!v.empty()) {
        PrintVector(v, out);
    }
    return out;
}

template<typename valT>
void PrintVector(const std::vector<valT>& input_vec, std::ostream& out_stream = std::cout) {
    size_t n = input_vec.size();
    out_stream << "|| ";
    for (size_t i = 0; i + 1 < n; ++i) {
        out_stream << input_vec[i] << ", ";
    }
    out_stream << input_vec[n - 1] << " ||";
}

template<typename valT>
void PrintRowMatrix(const std::vector<std::vector<valT>>& input_vec, std::ostream& out_stream = std::cout) {
    size_t rows_num = input_vec.size();
    size_t columns_num = input_vec[0].size();
    for (size_t i = 0; i + 1 < rows_num; ++i) {
        PrintVector(input_vec[i], out_stream);
        out_stream << '\n';
    }
    PrintVector(input_vec[rows_num - 1], out_stream);
}

template<typename intT>
size_t PrintAllLatticies(size_t dim, intT det, std::ostream& out_stream = std::cout, intT starting_value = 0) {
    
    SquareHermiteMatrixEnumerator<intT> m_enum(dim, det, starting_value);
    m_enum.Init();

    size_t count = 0;
    do {
        PrintRowMatrix(m_enum.GenerateRowsMatrix(), out_stream);
        out_stream << "\n\n";
        ++count;
    } while (m_enum.next());
    return count;
}